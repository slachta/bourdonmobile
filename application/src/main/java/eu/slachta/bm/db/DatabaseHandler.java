package eu.slachta.bm.db;

import java.util.ArrayList;
import java.util.List;

import eu.slachta.bm.model.BourdonMobileTestPageData;
import eu.slachta.bm.model.BourdonMobileTestInformation;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.*;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	private SQLiteDatabase db;
	
	// -----------------------------------
	// Database info
	// -----------------------------------
	
	public static final int DB_VERSION = 7;
	private static final String DB_NAME = "bmtests.db";

	// -----------------------------------
	// Table tests
	// -----------------------------------

	public static final String TABLE_TESTS = "tests";
	public static final String COL_TESTS_ID = "test_id";
	public static final String COL_TESTS_USERID = "user_id";
	public static final String COL_TESTS_IMGPERPAGE = "imagesPerPage";
	public static final String COL_TESTS_TIMEPERPAGE = "timePerPage";
	public static final String COL_TESTS_PAGES = "pages";
	public static final String COL_TESTS_DATE = "date";
	public static final String COL_TESTS_AGE = "age";
	public static final String COL_TESTS_SEX = "sex";
	public static final String COL_TESTS_EDUCATION_LEVEL = "educationLevel";
	public static final String COL_TESTS_NOTE = "note";
	public static final String COL_TESTS_DISPLAY_RESOLUTION = "resolution";
	public static final String COL_TESTS_DISPLAY_DPI = "dpi";
	public static final String COL_TESTS_DISPLAY_ORIENTATION = "orientation";
	public static final String COL_TESTS_DISPLAY_CELL_SIZE = "cellSize";
	public static final String COL_TESTS_REFIMG1ID = "refImg1Id";
	public static final String COL_TESTS_REFIMG2ID = "refImg2Id";
	public static final String COL_TESTS_REFIMG3ID = "refImg3Id";
	
	// -----------------------------------
	// Table testsData
	// -----------------------------------
		
	public static final String TABLE_TESTDATA = "testData";
	public static final String COL_TESTDATA_ID = "page_id";
	public static final String COL_TESTDATA_TESTID = COL_TESTS_ID;
	public static final String COL_TESTDATA_PAGENUM = "pageNumber";
	public static final String COL_TESTDATA_WRONGSETCOUNT = "wrongSetCount";
	public static final String COL_TESTDATA_RIGHTSETCOUNT = "rightSetCount";
	public static final String COL_TESTDATA_WRONGALLCOUNT = "wrongAllCount";
	public static final String COL_TESTDATA_RIGHTALLCOUNT = "rightAllCount";
	public static final String COL_TESTDATA_TIMESPENT = "timeSpent";
	
	private static final String CREATE_TABLE_TESTSDATA = "CREATE TABLE "
			+ TABLE_TESTDATA + " (" 
			+ COL_TESTDATA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
			+ COL_TESTDATA_TESTID + " INTEGER, "
			+ COL_TESTDATA_PAGENUM + " INTEGER,"
			+ COL_TESTDATA_WRONGSETCOUNT + " INTEGER," 
			+ COL_TESTDATA_RIGHTSETCOUNT + " INTEGER," 
			+ COL_TESTDATA_WRONGALLCOUNT + " INTEGER,"
			+ COL_TESTDATA_RIGHTALLCOUNT + " INTEGER," 
			+ COL_TESTDATA_TIMESPENT + " INTEGER,"
			+ "FOREIGN KEY("+COL_TESTDATA_TESTID+") REFERENCES "+TABLE_TESTS+"("+COL_TESTS_ID+") ON DELETE CASCADE);";
	
	private static final String CREATE_TABLE_TESTS = "CREATE TABLE "
			+ TABLE_TESTS + " (" 
			+ COL_TESTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
			+ COL_TESTS_USERID + " VARCHAR, "
			+ COL_TESTS_IMGPERPAGE + " INTEGER, " 
			+ COL_TESTS_TIMEPERPAGE + " INTEGER,"
			+ COL_TESTS_PAGES + " INTEGER,"
			+ COL_TESTS_DATE + " INTEGER,"
			+ COL_TESTS_AGE + " INTEGER,"
			+ COL_TESTS_SEX + " INTEGER,"
			+ COL_TESTS_EDUCATION_LEVEL + " INTEGER,"
			+ COL_TESTS_NOTE + " VARCHAR,"
			+ COL_TESTS_DISPLAY_RESOLUTION + " VARCHAR,"
			+ COL_TESTS_DISPLAY_DPI + " INTEGER,"
			+ COL_TESTS_DISPLAY_ORIENTATION + "INTEGER,"
			+ COL_TESTS_DISPLAY_CELL_SIZE + " INTEGER,"
			+ COL_TESTS_REFIMG1ID +" INTEGER,"
			+ COL_TESTS_REFIMG2ID +" INTEGER,"
			+ COL_TESTS_REFIMG3ID +" INTEGER);";

	public DatabaseHandler(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("PRAGMA foreign_keys = ON;");
		db.execSQL(CREATE_TABLE_TESTS);
		db.execSQL(CREATE_TABLE_TESTSDATA);
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
	    super.onOpen(db);
	    if (!db.isReadOnly()) {
	        db.execSQL("PRAGMA foreign_keys=ON;");
	    }
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DatabaseHandler.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TESTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TESTDATA);
		onCreate(db);
	}

	/**
	 * Returns a test with given ID.
	 *
     * @param id    ID of specific test
	 * @return      specific test with given ID
	 */

	public BourdonMobileTestInformation getTest(long id) {
		db = this.getReadableDatabase();
		String query = "SELECT * FROM tests WHERE " + COL_TESTS_ID + "=" + id;
		Cursor cursor = db.rawQuery(query, null);

		if (cursor != null)
			cursor.moveToFirst();

		BourdonMobileTestInformation test = new BourdonMobileTestInformation();
		test.setTestId(Integer.parseInt(cursor.getString(0)));
		test.setUserId(cursor.getString(1));
		test.setImagesPerPage(Integer.parseInt(cursor.getString(2)));
		test.setTimePerPage(Integer.parseInt(cursor.getString(3)));
		test.setPages(Integer.parseInt(cursor.getString(4)));
		test.setTime(Long.parseLong(cursor.getString(5)));
		
		test.setAge(Integer.parseInt(cursor.getString(6)));
		test.setSex(Integer.parseInt(cursor.getString(7)));
		test.setEducationLevel(Integer.parseInt(cursor.getString(8)));
		test.setNote(cursor.getString(9));
		
		test.setResolution(cursor.getString(10));
		test.setDpi(Integer.parseInt(cursor.getString(11)));
		test.setOrientation(Integer.parseInt(cursor.getString(12)));
		test.setCellSize(Integer.parseInt(cursor.getString(13)));
		
		test.setRefImg1Id(Integer.parseInt(cursor.getString(14)));
		test.setRefImg2Id(Integer.parseInt(cursor.getString(15)));
		test.setRefImg3Id(Integer.parseInt(cursor.getString(16)));
		
		cursor.close();
		db.close();
		
		return test;
	}

	/**
	 * Returns list of all tests
	 * 
	 * @return  the ArrayList containing BourdonMobileTestInformation of all tests.
	 */

	public ArrayList<BourdonMobileTestInformation> getAllTests() {
		ArrayList<BourdonMobileTestInformation> tests = new ArrayList<BourdonMobileTestInformation>();
		String selectQuery = "SELECT  * FROM " + TABLE_TESTS;

		db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				BourdonMobileTestInformation test = new BourdonMobileTestInformation();
				test.setTestId(Integer.parseInt(cursor.getString(0)));
				test.setUserId(cursor.getString(1));
				test.setImagesPerPage(Integer.parseInt(cursor.getString(2)));
				test.setTimePerPage(Integer.parseInt(cursor.getString(3)));
				test.setPages(Integer.parseInt(cursor.getString(4)));
				test.setTime(Long.parseLong(cursor.getString(5)));
				
				test.setAge(Integer.parseInt(cursor.getString(6)));
				test.setSex(Integer.parseInt(cursor.getString(7)));
				test.setEducationLevel(Integer.parseInt(cursor.getString(8)));
				test.setNote(cursor.getString(9));
				
				test.setResolution(cursor.getString(10));
				test.setDpi(Integer.parseInt(cursor.getString(11)));
				test.setOrientation(Integer.parseInt(cursor.getString(12)));
				test.setCellSize(Integer.parseInt(cursor.getString(13)));
				
				test.setRefImg1Id(Integer.parseInt(cursor.getString(14)));
				test.setRefImg2Id(Integer.parseInt(cursor.getString(15)));
				test.setRefImg3Id(Integer.parseInt(cursor.getString(16)));

				tests.add(test);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		
		return tests;
	}

	/**
	 * Adds a test and test data into database.
	 * 
	 * @param test
	 * @param pageData
	 */

	public long addTest(BourdonMobileTestInformation test, List<BourdonMobileTestPageData> pageData) {

        String queryTest = "INSERT OR REPLACE INTO " + TABLE_TESTS +
                " VALUES " +
                " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String queryTestData = "INSERT OR REPLACE INTO " + TABLE_TESTDATA +
                " VALUES " +
                " ( ?, ?, ?, ?, ?, ?, ?, ?)";

		db = this.getWritableDatabase();
        db.beginTransactionNonExclusive();

        SQLiteStatement stmt = db.compileStatement(queryTest);
        stmt.bindNull(1);
        stmt.bindString(2, test.getUserId());
        stmt.bindLong(3, test.getImagesPerPage());
        stmt.bindLong(4, test.getTimePerPage());
        stmt.bindLong(5, test.getPages());
        stmt.bindLong(6, test.getTime());
        stmt.bindLong(7, test.getAge());
        stmt.bindLong(8, test.getSex());
        stmt.bindLong(9, test.getEducationLevel());
        stmt.bindString(10, test.getNote());
        stmt.bindString(11, test.getResolution());
        stmt.bindLong(12, test.getDpi());
        stmt.bindLong(13, test.getOrientation());
        stmt.bindLong(14, test.getCellSize());
        stmt.bindLong(15, test.getRefImg1Id());
        stmt.bindLong(16, test.getRefImg2Id());
        stmt.bindLong(17, test.getRefImg3Id());

        stmt.execute();
        stmt.clearBindings();

		Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
		if (cursor != null)
			cursor.moveToFirst();

		long foreign_key_testid = Long.parseLong(cursor.getString(0));
        stmt = db.compileStatement(queryTestData);
		
		for (int i = 0; i < pageData.size(); i++) {
            stmt.bindNull(1);
            stmt.bindLong(2, foreign_key_testid);
            stmt.bindLong(3, pageData.get(i).getPageNumber());
            stmt.bindLong(4, pageData.get(i).getWrongSetCount());
            stmt.bindLong(5, pageData.get(i).getRightSetCount());
            stmt.bindLong(6, pageData.get(i).getWrongAllCount());
            stmt.bindLong(7, pageData.get(i).getRightAllCount());
            stmt.bindLong(8, pageData.get(i).getTimeSpent());
            stmt.execute();
            stmt.clearBindings();
		}

		Log.d("[BourdonMobile]", "Test added into database - test id is "+foreign_key_testid);
		cursor.close();
        db.setTransactionSuccessful();
        db.endTransaction();

        db.close();
		
		return foreign_key_testid;
	}

	/**
	 * Returns test data for specific test.
	 * 
	 * @param testID    the ID of specific test
	 * @return          the ArrayList of containing BourdonMobileTestPageData objects
	 */

	public ArrayList<BourdonMobileTestPageData> getTestData(long testID) {
		ArrayList<BourdonMobileTestPageData> pages = new ArrayList<BourdonMobileTestPageData>();
		String selectQuery = "SELECT  * FROM " + TABLE_TESTDATA + " WHERE test_id="+testID ;

		db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				BourdonMobileTestPageData page = new BourdonMobileTestPageData();
				
				page.setPageId(Long.parseLong(cursor.getString(0)));
				page.setTestId(Long.parseLong(cursor.getString(1)));
				page.setPageNumber(Integer.parseInt(cursor.getString(2)));
				page.setWrongSetCount(Integer.parseInt(cursor.getString(3)));
				page.setRightSetCount(Integer.parseInt(cursor.getString(4)));
				page.setWrongAllCount(Integer.parseInt(cursor.getString(5)));
				page.setRightAllCount(Integer.parseInt(cursor.getString(6)));
				page.setTimeSpent(Integer.parseInt(cursor.getString(7)));

				pages.add(page);
			} while (cursor.moveToNext());
		}
		
		cursor.close();
		db.close();
		return pages;
	}
	
	/**
	 * Returns all test data from all tests.
	 *
	 * @return the ArrayList containing BourdonMobileTestPageData objects for all tests
	 */

	public ArrayList<BourdonMobileTestPageData> getAllTestData() {
		ArrayList<BourdonMobileTestPageData> pages = new ArrayList<BourdonMobileTestPageData>();
		String selectQuery = "SELECT  * FROM " + TABLE_TESTDATA;

		db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				BourdonMobileTestPageData page = new BourdonMobileTestPageData();
				
				page.setPageId(Long.parseLong(cursor.getString(0)));
				page.setTestId(Long.parseLong(cursor.getString(1)));
				page.setPageNumber(Integer.parseInt(cursor.getString(2)));
				page.setWrongSetCount(Integer.parseInt(cursor.getString(3)));
				page.setRightSetCount(Integer.parseInt(cursor.getString(4)));
				page.setWrongAllCount(Integer.parseInt(cursor.getString(5)));
				page.setRightAllCount(Integer.parseInt(cursor.getString(6)));
				page.setTimeSpent(Integer.parseInt(cursor.getString(7)));

				pages.add(page);
			} while (cursor.moveToNext());
		}
		
		cursor.close();
		db.close();
		return pages;
	}
	
	public void deleteTest(long id) {
		db = this.getWritableDatabase();
		db.execSQL("DELETE FROM "+TABLE_TESTS+" WHERE "+COL_TESTS_ID+"="+id);
		Log.d("[BourdonMobile]", "Row id "+id+" deleted.");
		db.close();
	}
}
