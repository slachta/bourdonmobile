package eu.slachta.bm.device;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

public class Functions {

	public static boolean isPackageInstalled(String packagename, Context context) {
		PackageManager pm = context.getPackageManager();
		try {
			pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
			return true;
		} catch (NameNotFoundException e) {
			return false;
		}
	}
	
	public static boolean isApiKeyValid(String apiKey){
		return apiKey.matches("^([0-1]{1})-([A-Z0-9]{40})");
	}
	
	public static boolean isUserKeyValid(String userKey){
		return userKey.matches("^([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})-([A-Z0-9]{4})");
	}
}
