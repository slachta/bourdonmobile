package eu.slachta.bm.rest;

import java.util.HashMap;
import java.util.Map;

import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.StringRequest;

public class PostRequest extends StringRequest {

	private String data;
	private String apiKey;
	private String password;
	private String username;

	public PostRequest(int method, String url, Listener<String> listener, ErrorListener errorListener, String data,
			String apiKey, String username, String password) {
		super(method, url, listener, errorListener);
		this.setPassword(password);
		this.setUsername(username);
		this.setData(data);
		this.setApiKey(apiKey);
	}

	@Override
	protected Map<String, String> getParams() {
		Map<String, String> params = new HashMap<String, String>();

		params.put("bmTestsData", this.getData());
		params.put("apiKey", this.getApiKey());

		return params;
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		if (this.password != null && this.username != null && this.username.length() > 0 && this.password.length() > 0) {
			HashMap<String, String> params = new HashMap<String, String>();
			String creds = String.format("%s:%s", this.username, this.password);
			String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
			params.put("Authorization", auth);
			return params;
		} else {
			return super.getHeaders();
		}
	}

	/**
	 * @return the data
	 */
	private String getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	private void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the apiKey
	 */
	private String getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiKey
	 *            the apiKey to set
	 */
	private void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
}
