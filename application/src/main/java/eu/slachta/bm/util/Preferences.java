package eu.slachta.bm.util;

import eu.slachta.bm.R;

public class Preferences {
	private final static int[] images = {
            R.drawable.im_0_normal,
            R.drawable.im_1_normal,
			R.drawable.im_2_normal,
            R.drawable.im_3_normal,
			R.drawable.im_4_normal,
            R.drawable.im_5_normal,
			R.drawable.im_6_normal,
            R.drawable.im_7_normal,
    };

	/**
	 * @return the images
	 */
	public static int[] getImages() {
		return images;
	}

	public static Integer getAssociatedImage(int image) {
		switch (image) {
        case R.drawable.im_0_normal:
            return R.drawable.im_0_selected;
		case R.drawable.im_1_normal:
			return R.drawable.im_1_selected;
		case R.drawable.im_2_normal:
			return R.drawable.im_2_selected;
		case R.drawable.im_3_normal:
			return R.drawable.im_3_selected;
		case R.drawable.im_4_normal:
			return R.drawable.im_4_selected;
		case R.drawable.im_5_normal:
			return R.drawable.im_5_selected;
		case R.drawable.im_6_normal:
			return R.drawable.im_6_selected;
		case R.drawable.im_7_normal:
			return R.drawable.im_7_selected;
		default:
			return R.drawable.img_r144_false;
		}
	}

	public static Integer getAssociatedTrueImage(int image) {
		switch (image) {
        case R.drawable.im_0_normal:
            return R.drawable.im_0_true;
		case R.drawable.im_1_normal:
			return R.drawable.im_1_true;
		case R.drawable.im_2_normal:
			return R.drawable.im_2_true;
		case R.drawable.im_3_normal:
			return R.drawable.im_3_true;
		case R.drawable.im_4_normal:
			return R.drawable.im_4_true;
		case R.drawable.im_5_normal:
			return R.drawable.im_5_true;
		case R.drawable.im_6_normal:
			return R.drawable.im_6_true;
		case R.drawable.im_7_normal:
			return R.drawable.im_7_true;
		default:
			break;
		}

		return -1;
	}

	public static Integer getAssociatedFalseImage(int image) {
		switch (image) {
        case R.drawable.im_0_normal:
            return R.drawable.im_0_false;
		case R.drawable.im_1_normal:
			return R.drawable.im_1_false;
		case R.drawable.im_2_normal:
			return R.drawable.im_2_false;
		case R.drawable.im_3_normal:
			return R.drawable.im_3_false;
		case R.drawable.im_4_normal:
			return R.drawable.im_4_false;
		case R.drawable.im_5_normal:
			return R.drawable.im_5_false;
		case R.drawable.im_6_normal:
			return R.drawable.im_6_false;
		case R.drawable.im_7_normal:
			return R.drawable.im_7_false;
		}
		return image;
	}

	public static int getImageById(Integer id) {
		switch (id) {
		case 0:
			return R.drawable.im_0_normal;
		case 1:
			return R.drawable.im_1_normal;
		case 2:
			return R.drawable.im_2_normal;
		case 3:
			return R.drawable.im_3_normal;
		case 4:
			return R.drawable.im_4_normal;
		case 5:
			return R.drawable.im_5_normal;
		case 6:
			return R.drawable.im_6_normal;
		case 7:
			return R.drawable.im_7_normal;
		default:
			return R.drawable.img_r144_false;
		}
	}

	public static int getProperImageId(Integer viewId) {
		switch (viewId) {
		case R.drawable.im_0_normal:
			return 0;
		case R.drawable.im_1_normal:
			return 1;
		case R.drawable.im_2_normal:
			return 2;
		case R.drawable.im_3_normal:
			return 3;
		case R.drawable.im_4_normal:
			return 4;
		case R.drawable.im_5_normal:
			return 5;
		case R.drawable.im_6_normal:
			return 6;
		case R.drawable.im_7_normal:
			return 7;
		default:
			break;
		}

		return -1;
	}

}
