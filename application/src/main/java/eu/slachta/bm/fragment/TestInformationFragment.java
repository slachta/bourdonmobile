package eu.slachta.bm.fragment;

import eu.slachta.bm.R;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TestInformationFragment extends Fragment {
	
	public TestInformationFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_test_information, container, false);
        
        TextView tv = (TextView) rootView.findViewById(R.id.test_info_textview);
        tv.setText(Html.fromHtml(getResources().getString(R.string.test_info)));
         
        return rootView;
    }
}
