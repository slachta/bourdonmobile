package eu.slachta.bm.fragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import eu.slachta.bm.activity.MainActivity;
import eu.slachta.bm.R;
import eu.slachta.bm.activity.ShowAdvancedResultsActivity;
import eu.slachta.bm.adapter.ResultsAdapter;
import eu.slachta.bm.db.DatabaseHandler;
import eu.slachta.bm.model.BourdonMobileTest;
import eu.slachta.bm.model.BourdonMobileTestPageData;
import eu.slachta.bm.model.BourdonMobileTestInformation;
import eu.slachta.bm.rest.PostRequest;

public class ResultsFragment extends Fragment {
	private DatabaseHandler db;
	private ArrayList<BourdonMobileTestInformation> pd;
	private ResultsAdapter adapter;
	private ActionMode mmode;
	private ListView list = null;
	private View rootView = null;
	private RequestQueue queue;

	public ResultsFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		db = new DatabaseHandler(getActivity());
		pd = db.getAllTests();

		if (pd.size() == 0) {
			rootView = inflater.inflate(R.layout.fragment_empty_results,
					container, false);
		} else {
			rootView = inflater.inflate(R.layout.fragment_results, container,
					false);

			adapter = new ResultsAdapter(rootView.getContext(), pd);
			adapter.notifyDataSetChanged();

			list = (ListView) rootView.findViewById(R.id.results_listview);
			list.setAdapter(adapter);
			list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			list.setMultiChoiceModeListener(new MultiChoiceModeListener() {

				@Override
				public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
					return false;
				}

				@Override
				public void onDestroyActionMode(ActionMode mode) {
					// TODO Auto-generated method stub
				}

				@Override
				public boolean onCreateActionMode(ActionMode mode, Menu menu) {
					MenuInflater inflater = getActivity().getMenuInflater();
					inflater.inflate(R.menu.contextual_menu, menu);
					mmode = mode;
					return true;
				}

				@Override
				public boolean onActionItemClicked(ActionMode mode,
						MenuItem item) {
					SparseBooleanArray sba = list.getCheckedItemPositions();
					switch (item.getItemId()) {
					case R.id.results_items_delete:
						for (int i = 0; i < sba.size(); i++) {
							db.deleteTest(pd.get(sba.keyAt(i)).getTestId());
						}
						pd = db.getAllTests();
						adapter.refreshData(pd);
						list.setAdapter(adapter);
						mode.finish();
						return true;
					default:
						return false;
					}
				}

				@Override
				public void onItemCheckedStateChanged(ActionMode mode,
						int position, long id, boolean checked) {
					// TODO Auto-generated method stub
				}
			});

			list.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					list.setItemChecked(position,
							!adapter.isPositionChecked(position));

					return false;
				}
			});

			list.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long id) {
					Intent myIntent = new Intent(arg0.getContext(),
							ShowAdvancedResultsActivity.class);
					myIntent.putExtra("test_id", pd.get(position).getTestId());
					myIntent.putExtra("state", 1);
					startActivityForResult(myIntent, 0);
				}
			});
		}

		return rootView;
	}
	

	@Override
	public void onStop() {
		if (this.mmode != null) {
			this.mmode.finish();
		}
		super.onStop();
	}
	
	private void toast(String message){
		Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onDetach() {
		if(queue != null){
			queue.cancelAll("bmRest");
		}
		super.onDetach();
	}
}
