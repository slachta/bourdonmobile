package eu.slachta.bm.fragment;

import eu.slachta.bm.R;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutApplicationFragment extends Fragment {
	
	public AboutApplicationFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about_application, container, false);
        
        TextView aboutAppHeader = (TextView) rootView.findViewById(R.id.about_textview_info_header);
        TextView aboutAppContent = (TextView) rootView.findViewById(R.id.about_textview_info_content);
        aboutAppHeader.setText(Html.fromHtml("<h1>"+getResources().getString(R.string.app_name)+"</h1>"));
        aboutAppContent.setText(Html.fromHtml(getResources().getString(R.string.about_app_version_text)+" "+getResources().getString(R.string.app_version)));
        
        TextView aboutAppMoreInfoHeader = (TextView) rootView.findViewById(R.id.about_textview_more_info_header);
        TextView aboutAppMoreInfoContent = (TextView) rootView.findViewById(R.id.about_textview_more_info_content);
        
        aboutAppMoreInfoHeader.setText(Html.fromHtml(getResources().getString(R.string.about_textview_more_info_header)));
        aboutAppMoreInfoContent.setText(Html.fromHtml(getResources().getString(R.string.about_textview_more_info_content)));

        return rootView;
    }
}
