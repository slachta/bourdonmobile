package eu.slachta.bm.fragment;

import eu.slachta.bm.R;
import eu.slachta.bm.activity.FullTestActivity;
import eu.slachta.bm.activity.WorkoutActivity;
import eu.slachta.bm.model.FormAndDeviceData;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TestFragment extends Fragment {
	
	public TestFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_bm_test, container, false);
        
        TextView header = (TextView) rootView.findViewById(R.id.test_fragment_test_header);
        TextView content = (TextView) rootView.findViewById(R.id.test_fragment_test_info);
        
        String head = getResources().getString(R.string.test_selection_head);
        header.setText(Html.fromHtml(head));
        
        String summary = getResources().getString(R.string.test_selection_introduction);
        content.setText(Html.fromHtml(summary));
        
        Button training = (Button) rootView.findViewById(R.id.test_start_training_button);
        training.setOnClickListener(new TrainingListener());
        
        Button test = (Button) rootView.findViewById(R.id.test_start_test_button);
        test.setOnClickListener(new TestListener());
         
        return rootView;
    }
	
	class TrainingListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(v.getContext(), WorkoutActivity.class);
			startActivityForResult(intent, 0);
		}

	}
	
	class TestListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(v.getContext(), FullTestActivity.class);
			intent.putExtra("formAndDeviceData", getAnonymousFormData());
			startActivityForResult(intent, 0);
		}

	}

	private FormAndDeviceData getAnonymousFormData(){
		FormAndDeviceData fadd = new FormAndDeviceData();

		fadd.setAge(0);
		fadd.setSex(0);
		fadd.setEducationLevel(0);
		fadd.setNote(getResources().getString(R.string.anonymous_test));
		fadd.setUserid(getResources().getString(R.string.anonymous_user_id));

		return fadd;
	}
}
