package eu.slachta.bm.fragment;

import eu.slachta.bm.R;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.Preference.OnPreferenceClickListener;
import android.widget.Toast;

public class TestSettingsFragment extends PreferenceFragment {
	
	public TestSettingsFragment(){}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.fragment_bm_settings);
		
		Preference defaultPreference = (Preference) findPreference("bm_defaults");

		defaultPreference
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						Editor editor = PreferenceManager
								.getDefaultSharedPreferences(
										getActivity().getApplicationContext()).edit();
						editor.putString("bm_time_per_page", Integer.toString(getResources().getInteger(R.integer.test_time_per_page)));
						editor.putString("bm_number_of_pages", Integer.toString(getResources().getInteger(R.integer.test_number_of_pages)));
						editor.putString("bm_number_of_images_per_page", Integer.toString(getResources().getInteger(R.integer.test_number_of_images_per_page)));
						editor.putString("bm_cell_size", Integer.toString(getResources().getInteger(R.integer.test_default_cell_size)));
						editor.commit();
						
						Toast.makeText(
								getActivity().getApplicationContext(),
								getResources().getString(
										R.string.bm_settings_defaults_info),
								Toast.LENGTH_SHORT).show();
						
						return false;
					}
				});
	}
}

