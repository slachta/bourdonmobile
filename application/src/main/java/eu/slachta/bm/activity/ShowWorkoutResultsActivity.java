package eu.slachta.bm.activity;

import java.math.BigDecimal;
import java.math.RoundingMode;

import eu.slachta.bm.R;
import eu.slachta.bm.model.Data;
import eu.slachta.bm.model.FormAndDeviceData;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

public class ShowWorkoutResultsActivity extends Activity implements OnClickListener{
	private Data data;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_results);
		TextView t = (TextView) findViewById(R.id.bm_simple_results_textview);
		Bundle b = getIntent().getExtras();
		
		data = (Data) b.getSerializable("dataPerAllPages");
		int[] prom = data.get(0);
		
		int timePerPage;
		
		if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_time_per_page","50000").equals("")){
			timePerPage = this.getResources().getInteger(R.integer.test_time_per_page);
		}else{
			timePerPage = Integer.parseInt(PreferenceManager
					.getDefaultSharedPreferences(this).getString(
							"bm_time_per_page",
							Integer.toString(this.getResources().getInteger(
									R.integer.test_time_per_page))));
		}
		
		StringBuilder stats = new StringBuilder();
		stats.append("<h2>"+getResources().getString(R.string.bm_simple_results_textview) + "</h2>");
		stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_right_images) + " " + prom[2] + "/" + prom[0] + "<br>");
		stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_wrong_images) + " " + prom[3] + "/" + prom[1] + "<br>");
		stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_fruitfulness) + " " + ((prom[0]!=0)?round((((double)prom[2]/(double)prom[0])*100),2):0) + "% <br>");
		stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_error_rate) + " " + ((prom[1]!=0)?round((((double)prom[3]/(double)prom[1])*100),2):0) + "% <br>");
		stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_time_per_page) + " " + (timePerPage - prom[4]) + " ms <br>");
			
		t.setText(Html.fromHtml(stats.toString()));
		
		Button selectTestButton = (Button) findViewById(R.id.bm_simple_results_button_do_workout);
		selectTestButton.setOnClickListener(this);
		
		Button goMenu = (Button) findViewById(R.id.bm_simple_results_button_do_test);
		goMenu.setOnClickListener(this);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	public void onClick(View v) {
		if(v == findViewById(R.id.bm_simple_results_button_do_workout)){
			Intent myIntent = new Intent(getBaseContext(),WorkoutActivity.class);
			startActivityForResult(myIntent, 0);
			finish();
		}else if(v == findViewById(R.id.bm_simple_results_button_do_test)){
			Intent intent = new Intent(v.getContext(), FullTestActivity.class);
			intent.putExtra("formAndDeviceData", getAnonymousFormData());
			startActivityForResult(intent, 0);
			finish();
		}
	}

	private FormAndDeviceData getAnonymousFormData(){
		FormAndDeviceData fadd = new FormAndDeviceData();

		fadd.setAge(0);
		fadd.setSex(0);
		fadd.setEducationLevel(0);
		fadd.setNote(getResources().getString(R.string.anonymous_test));
		fadd.setUserid(getResources().getString(R.string.anonymous_user_id));

		return fadd;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
	   switch (item.getItemId()) 
	   {
	     case android.R.id.home:
	        Intent intent = new Intent(this, MainActivity.class);
	        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	        startActivity(intent);
	        finish();
	        return true;
	     default:
	        return super.onOptionsItemSelected(item);
	   }
	}
	
	private double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
