package eu.slachta.bm.activity;

import java.math.BigDecimal;
import java.math.RoundingMode;

import eu.slachta.bm.R;
import eu.slachta.bm.db.DatabaseHandler;
import eu.slachta.bm.model.BourdonMobileTestInformation;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

public class StatisticsActivity extends Activity {
	private int[] TIMESPENT;
	private int[] WRONGSETCOUNT;
	private int[] RIGHTSETCOUNT;
	private int[] WRONGALLCOUNT;
	private int[] RIGHTALLCOUNT;
	private long TESTID;
	private DatabaseHandler db;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics);

		TIMESPENT = getIntent().getIntArrayExtra("timeSpent");
		WRONGSETCOUNT = getIntent().getIntArrayExtra("wrongSet");
		RIGHTSETCOUNT = getIntent().getIntArrayExtra("rightSet");
		WRONGALLCOUNT = getIntent().getIntArrayExtra("wrongAll");
		RIGHTALLCOUNT = getIntent().getIntArrayExtra("rightAll");
		TESTID = getIntent().getLongExtra("test_id", -1);
		
		db = new DatabaseHandler(this);
		BourdonMobileTestInformation t = db.getTest((int) TESTID);

		StringBuilder stats = new StringBuilder();

		int sumOfWrongSet = 0;
		int sumOfWrongAll = 0;
		int sumOfRightSet = 0; 
		int sumOfRightAll = 0;
		
		for (int i=0; i<WRONGSETCOUNT.length; i++){
			sumOfWrongSet+=WRONGSETCOUNT[i];
			sumOfWrongAll+=WRONGALLCOUNT[i];
			sumOfRightSet+=RIGHTSETCOUNT[i];
			sumOfRightAll+=RIGHTALLCOUNT[i];
		}
		
		stats.append("<h2>"+getResources().getString(R.string.stats_header_simplified_stats_global)+"</h2>");
		stats.append("&nbsp;&nbsp;"+ getResources().getString(R.string.stats_fruitfulness) + round((((double)sumOfRightSet/(double)sumOfRightAll)*100),2) + "% <br>");
		stats.append("&nbsp;&nbsp;"+ getResources().getString(R.string.stats_error_rate) + round((((double)sumOfWrongSet/(double)sumOfWrongAll)*100),2) + "% <br>");
		
		stats.append("<h2>"+getResources().getString(R.string.stats_header_simplified_stats_per_page)+"</h2>");
		for (int i = 0; i < RIGHTSETCOUNT.length; i++) {
			stats.append("<b>"+getResources().getString(R.string.stats_test_page) + " - " + (i + 1) + ".</b>:<br>");
			stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_right_images) + " " + RIGHTSETCOUNT[i]
					+ "/" + RIGHTALLCOUNT[i] + "<br>");
			stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_wrong_images) + " " + WRONGSETCOUNT[i] + "/"
					+ WRONGALLCOUNT[i] + "<br>");
			stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_fruitfulness) + " " + ((RIGHTALLCOUNT[i]!=0)?round((((double)RIGHTSETCOUNT[i]/(double)RIGHTALLCOUNT[i])*100),2):0) + "% <br>");
			stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_error_rate) + " " + ((WRONGALLCOUNT[i]!=0)?round((((double)WRONGSETCOUNT[i]/(double)WRONGALLCOUNT[i])*100),2):0) + "% <br>");
			stats.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_time_per_page) + " " + (TIMESPENT[i]) + " ms <br>");
		}

		TextView tv = (TextView) findViewById(R.id.statistics_txt);
		tv.setText(Html.fromHtml(stats.toString()));

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		String[] genders = getResources().getStringArray(
				R.array.test_form_required_info_genders);
		String[] eduLevels = getResources().getStringArray(
				R.array.test_form_required_info_education_levels);

		StringBuilder userAndTestInfo = new StringBuilder();

		userAndTestInfo.append("<h2>"+getResources().getString(R.string.stats_test_parameters2)+"</h2>");
		userAndTestInfo.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_test_date)+": "
				+ t.getDate() + "<br>");
		userAndTestInfo.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_test_number_of_pages)+": " + t.getPages()
				+ "<br>");
		userAndTestInfo.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_test_number_images_per_page)+": "
				+ t.getImagesPerPage() + "<br>");
		userAndTestInfo.append("&nbsp;&nbsp;"+getResources().getString(R.string.stats_test_time_per_page)+": "
				+ (t.getTimePerPage() / 1000) + " s.");

		TextView params = (TextView) findViewById(R.id.statistics_user_info);
		params.setText(Html.fromHtml(userAndTestInfo.toString()));

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
