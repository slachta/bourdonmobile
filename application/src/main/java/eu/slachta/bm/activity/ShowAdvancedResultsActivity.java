package eu.slachta.bm.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import eu.slachta.bm.R;
import eu.slachta.bm.db.DatabaseHandler;
import eu.slachta.bm.model.BourdonMobileTestInformation;
import eu.slachta.bm.model.BourdonMobileTestPageData;
import eu.slachta.bm.model.Data;
import eu.slachta.bm.model.FormAndDeviceData;

public class ShowAdvancedResultsActivity extends ListActivity {
	private String[] items;
	private String[] descriptionItems;
	public static String graphLegend;
	public static String graphLegend1;
	public static String graphLegend2;
	public int type;
	private boolean editable;

	// --------
	// Data testu
	// --------

	private Data d;
	private long test_id;
	private List<BourdonMobileTestPageData> pd;
	private BourdonMobileTestInformation bti;

	public static int[] WRONGSETCOUNT;
	public static int[] RIGHTSETCOUNT;
	public static int[] WRONGALLCOUNT;
	public static int[] RIGHTALLCOUNT;
	public static int[] TIMESPENT;

	private Bundle bundle;
	private DatabaseHandler db;
	private FormAndDeviceData formAndDeviceData;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.activity_advanced_results);

		// load slide menu items
		items = getResources().getStringArray(R.array.adv_results_items);
		descriptionItems = getResources().getStringArray(
				R.array.adv_results_items_description);

		setClassicAdapter();
		bundle = new Bundle();
		int state = getIntent().getIntExtra("state", -1);
		editable = true;

		formAndDeviceData = (FormAndDeviceData) getIntent().getSerializableExtra("formAndDeviceData");
		
		db = new DatabaseHandler(this);
		switch (state) {
		case 0: // from fulltest
			bundle = getIntent().getExtras();
			setClassicAdapter();
			d = (Data) bundle.getSerializable("dataPerAllPages");
			store(db, d, formAndDeviceData);
			pd = db.getTestData(test_id);
			bti = db.getTest(test_id);

			break;
		case 1: // from results fragment
			test_id = getIntent().getLongExtra("test_id", 1);
			pd = db.getTestData(test_id);
			bti = db.getTest(test_id);

			break;
		default:
			break;
		}

		bundle.clear();
		bundle.putSerializable("dataPerAllPages", d);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	private void store(DatabaseHandler db, Data d, FormAndDeviceData fd) {
		BourdonMobileTestInformation t = new BourdonMobileTestInformation();
		t.setImagesPerPage(fd.getNumberOfImagesPerPage());
		t.setPages(fd.getNumberOfPages());
		t.setTimePerPage(fd.getTimePerPage());
		t.setTime(new Date().getTime());
		// Form data
		t.setAge(fd.getAge());
		t.setSex(fd.getSex());
		t.setEducationLevel(fd.getEducationLevel());
		t.setNote((fd.getNote()=="") ? "empty note" : fd.getNote());
		// Device params
		t.setResolution(fd.getResolution());
		t.setDpi(fd.getDpi());
		t.setOrientation(fd.getOrientation());
		t.setCellSize(fd.getCellSize());
		t.setUserId(fd.getUserid());
		t.setRefImg1Id(fd.getRefImg1Id());
		t.setRefImg2Id(fd.getRefImg2Id());
		t.setRefImg3Id(fd.getRefImg3Id());

		List<BourdonMobileTestPageData> pages = new ArrayList<BourdonMobileTestPageData>();

		for (int i = 0; i < d.size(); i++) {
			pages.add(new BourdonMobileTestPageData(
					0, //page_id
					0, //test_id
					d.get(i)[5], //pageNumber 
					d.get(i)[3], //wrongSetCount
					d.get(i)[2], //rightSetCount
					d.get(i)[1], //wrongAllCount
					d.get(i)[0], //rightAllCount
					fd.getTimePerPage()-d.get(i)[4])); //timeSpent
		}

		test_id = db.addTest(t, pages);
		pages.clear();
		t = null;
	}

	private void setClassicAdapter() {
		setListAdapter(new AdvancedResultsMenuAdapter(ShowAdvancedResultsActivity.this,
				R.layout.activity_advanced_results_item, items));
	}

	public void onListItemClick(ListView parent, View v, int position, long id) {
		Intent myIntent;
		switch (position) {
		case 0:
			myIntent = new Intent(v.getContext(),
					StatisticsActivity.class);
			TIMESPENT = new int[pd.size()];
			RIGHTSETCOUNT = new int[pd.size()];
			WRONGSETCOUNT = new int[pd.size()];
			WRONGALLCOUNT = new int[pd.size()];
			RIGHTALLCOUNT = new int[pd.size()];

			for (int i = 0; i < pd.size(); i++) {
				TIMESPENT[i] = pd.get(i).getTimeSpent();
				RIGHTSETCOUNT[i] = pd.get(i).getRightSetCount();
				WRONGSETCOUNT[i] = pd.get(i).getWrongSetCount();
				WRONGALLCOUNT[i] = pd.get(i).getWrongAllCount();
				RIGHTALLCOUNT[i] = pd.get(i).getRightAllCount();
			}

			myIntent.putExtra("timeSpent", TIMESPENT);
			myIntent.putExtra("wrongAll", WRONGALLCOUNT);
			myIntent.putExtra("wrongSet", WRONGSETCOUNT);
			myIntent.putExtra("rightAll", RIGHTALLCOUNT);
			myIntent.putExtra("rightSet", RIGHTSETCOUNT);
			myIntent.putExtra("rightSet", RIGHTSETCOUNT);
			myIntent.putExtra("test_id", test_id);

			startActivityForResult(myIntent, 0);
			break;
		case 1:
			myIntent = new Intent(v.getContext(),
					ShowGraphActivity.class);
			TIMESPENT = new int[pd.size()];
			RIGHTSETCOUNT = new int[pd.size()];
			WRONGSETCOUNT = new int[pd.size()];

			for (int i = 0; i < pd.size(); i++) {
				TIMESPENT[i] = (int)((bti.getTimePerPage()!=0)?(((double)pd.get(i).getTimeSpent()/(double)bti.getTimePerPage())*100):0);
				RIGHTSETCOUNT[i] = (int)((pd.get(i).getRightAllCount()!=0)?(((double)pd.get(i).getRightSetCount()/(double)pd.get(i).getRightAllCount())*100):0);
				WRONGSETCOUNT[i] = (int)((pd.get(i).getWrongAllCount()!=0)?(((double)pd.get(i).getWrongSetCount()/(double)pd.get(i).getWrongAllCount())*100):0);
			}

			type = 0;

			myIntent.putExtra("type", type);
			myIntent.putExtra("timeSpent", TIMESPENT);
			myIntent.putExtra("rightSet", RIGHTSETCOUNT);
			myIntent.putExtra("wrongSet", WRONGSETCOUNT);
			TIMESPENT = null;
			RIGHTSETCOUNT = null;
			WRONGSETCOUNT = null;
			startActivityForResult(myIntent, 0);
			break;
		case 2:
			myIntent = new Intent(v.getContext(),
					ShowGraphActivity.class);

			WRONGSETCOUNT = new int[pd.size()];
			WRONGALLCOUNT = new int[pd.size()];

			for (int i = 0; i < pd.size(); i++) {
				WRONGSETCOUNT[i] = pd.get(i).getWrongSetCount();
				WRONGALLCOUNT[i] = pd.get(i).getWrongAllCount();
			}

			type = 1;

			myIntent.putExtra("type", type);
			myIntent.putExtra("wrongAll", WRONGALLCOUNT);
			myIntent.putExtra("wrongSet", WRONGSETCOUNT);
			WRONGSETCOUNT = null;
			WRONGALLCOUNT = null;
			startActivityForResult(myIntent, 0);
			break;
		case 3:
			myIntent = new Intent(v.getContext(),
					ShowGraphActivity.class);

			RIGHTSETCOUNT = new int[pd.size()];
			RIGHTALLCOUNT = new int[pd.size()];

			for (int i = 0; i < pd.size(); i++) {
				RIGHTSETCOUNT[i] = pd.get(i).getRightSetCount();
				RIGHTALLCOUNT[i] = pd.get(i).getRightAllCount();
			}

			type = 2;

			myIntent.putExtra("type", type);
			myIntent.putExtra("rightAll", RIGHTALLCOUNT);
			myIntent.putExtra("rightSet", RIGHTSETCOUNT);
			RIGHTSETCOUNT = null;
			RIGHTALLCOUNT = null;
			startActivityForResult(myIntent, 0);
			break;
		case 4:
			myIntent = new Intent(v.getContext(),
					ShowGraphActivity.class);

			TIMESPENT = new int[pd.size()];

			for (int i = 0; i < pd.size(); i++) {
				TIMESPENT[i] = bti.getTimePerPage()-pd.get(i).getTimeSpent();
			}

			type = 3;

			myIntent.putExtra("type", type);
			myIntent.putExtra("timeSpent", TIMESPENT);
			myIntent.putExtra("timePerPage", bti.getTimePerPage());
			TIMESPENT = null;

			startActivityForResult(myIntent, 0);
			break;
		default:
			break;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public class AdvancedResultsMenuAdapter extends ArrayAdapter<String> {

		public AdvancedResultsMenuAdapter(Context context, int textViewResourceId, String[] objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public boolean isEnabled(int position) {
			if (editable) {
				return true;
			} else {
				return false;
			}
		}

		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.activity_advanced_results_item, parent, false);
			TextView label = (TextView) row.findViewById(R.id.activity_advanced_results_item_head);
			TextView podlabel = (TextView) row.findViewById(R.id.activity_advanced_results_item_content);
			label.setText(items[position]);
			podlabel.setText(descriptionItems[position]);

			ImageView icon = (ImageView) row.findViewById(R.id.activity_advanced_results_item_icon);

			switch (position) {
			case 0:
				icon.setImageResource(R.drawable.ar_notepad);
				break;
			case 1:
				icon.setImageResource(R.drawable.ar_parvar);
				break;
			case 2:
				icon.setImageResource(R.drawable.ar_graf1);
				break;
			case 3:
				icon.setImageResource(R.drawable.ar_graf2);
				break;
			case 4:
				icon.setImageResource(R.drawable.ar_graf3);
				break;
			default:
				break;
			}

			return row;
		}
	}

}
