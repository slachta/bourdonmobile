package eu.slachta.bm.activity;

import java.util.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.graphics.*;
import android.os.*;
import android.preference.PreferenceManager;
import android.text.*;
import android.util.DisplayMetrics;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.*;

import eu.slachta.bm.util.Preferences;
import eu.slachta.bm.R;
import eu.slachta.bm.adapter.*;
import eu.slachta.bm.model.Data;

public class WorkoutActivity extends Activity {

	// ----
	// Test variables, objects and arrays
	// ----

	private Vector<Integer> AssignedImage;
	private int ARRAY_SIZE;
	private Integer[] mThumbIds;
	private boolean[] isItemSelected;
	private CountDownTimer timer;
	protected int first;
	protected int firstId;
	protected int second;
	protected int secondId;
	protected int third;
	protected int thirdId;

	// ----
	// GUI components
	// ----

	private TextView countDownTextView;
	private Button but1;
	private ImageView im1;
	private ImageView im2;
	private ImageView im3;
	protected GridView gridview;

	// ----
	// Test data variables (per page)
	// ----

	private int rightCount = 0;
	private int wrongCount = 0;
	private int rightSelectedCount = 0;
	private int wrongSelectedCount = 0;
	private int testDuration = 0;
	private int testIteration = 1;
	
	private int timePerPage;

	// ----
	// Cumulative variables
	// ----

	private Bundle b;
	private Data seznamMereni;

	// ----
	// GUI params
	// ----

	private int cellSize = 75;

	// ----
	// On Create
	// ----

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getActionBar().hide();
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workout);
		
		if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_number_of_images_per_page","30").equals("")){
			this.ARRAY_SIZE = this.getResources().getInteger(R.integer.test_number_of_images_per_page);
		}else{
			this.ARRAY_SIZE = Integer.parseInt(PreferenceManager
					.getDefaultSharedPreferences(this).getString(
							"bm_number_of_images_per_page",
							Integer.toString(this.getResources().getInteger(
									R.integer.test_number_of_images_per_page))));
		}
		
		mThumbIds = new Integer[ARRAY_SIZE];
		isItemSelected = new boolean[ARRAY_SIZE];
		
		if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_time_per_page","50000").equals("")){
			this.timePerPage = this.getResources().getInteger(R.integer.test_time_per_page);
		}else{
			this.timePerPage = Integer.parseInt(PreferenceManager
					.getDefaultSharedPreferences(this).getString(
							"bm_time_per_page",
							Integer.toString(this.getResources().getInteger(
									R.integer.test_time_per_page))));
		}

		if (seznamMereni == null) {
			initMain();
		}

		TextView tv1 = (TextView) findViewById(R.id.workout_preview_textview_description);
		tv1.setText(Html.fromHtml(getResources().getString(R.string.test_header)));

		im1 = (ImageView) findViewById(R.id.workout_imageprev_1_imageview);
		first = AssignedImage.elementAt(1);
		firstId = Preferences.getProperImageId(first);
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), first);
		im1.setImageBitmap(bmp);
		im1.invalidate();

		im2 = (ImageView) findViewById(R.id.workout_imageprev_2_imageview);
		second = AssignedImage.elementAt(2);
		secondId = Preferences.getProperImageId(second);
		bmp = BitmapFactory.decodeResource(getResources(), second);
		im2.setImageBitmap(bmp);
		im2.invalidate();

		im3 = (ImageView) findViewById(R.id.workout_imageprev_3_imageview);
		third = AssignedImage.elementAt(3);
		thirdId = Preferences.getProperImageId(third);
		bmp = BitmapFactory.decodeResource(getResources(), third);
		im3.setImageBitmap(bmp);
		im3.invalidate();

		gridview = (GridView) findViewById(R.id.GridView01);

		generateArrayOfImages();
		applySettings();

		gridview.setAdapter(new ImageAdapter(this, mThumbIds, cellSize));
		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				if (v.getId() == firstId) {
					if (!isItemSelected[position]) {
						mThumbIds[position] = Preferences
								.getAssociatedTrueImage(first);
						gridview.setAdapter(new ImageAdapter(getBaseContext(),
								mThumbIds, cellSize));
						gridview.setSelection(position);
						isItemSelected[position] = true;
						setNumberOfRightSelectedImages(getNumberOfRightSelectedImages() + 1);
					}
				} else if (v.getId() == secondId) {
					if (!isItemSelected[position]) {
						mThumbIds[position] = Preferences
								.getAssociatedTrueImage(second);
						gridview.setAdapter(new ImageAdapter(getBaseContext(),
								mThumbIds, cellSize));
						gridview.setSelection(position);
						isItemSelected[position] = true;
						setNumberOfRightSelectedImages(getNumberOfRightSelectedImages() + 1);
					}
				} else if (v.getId() == thirdId) {
					if (!isItemSelected[position]) {
						mThumbIds[position] = Preferences
								.getAssociatedTrueImage(third);
						gridview.setAdapter(new ImageAdapter(getBaseContext(),
								mThumbIds, cellSize));
						gridview.setSelection(position);
						isItemSelected[position] = true;
						setNumberOfRightSelectedImages(getNumberOfRightSelectedImages() + 1);
					}
				} else {
					if (!isItemSelected[position]) {
						mThumbIds[position] = Preferences
								.getAssociatedFalseImage(Preferences
										.getImageById(v.getId()));
						gridview.setAdapter(new ImageAdapter(getBaseContext(),
								mThumbIds, cellSize));
						gridview.setSelection(position);
						isItemSelected[position] = true;
						setNumberOfWrongSelectedImages(getNumberOfWrongSelectedImages() + 1);
					}
				}
			}
		});

		countDownTextView = (TextView) findViewById(R.id.workout_counter);

		but1 = (Button) findViewById(R.id.workout_button_interrupt_test);
		but1.setText(getResources().getString(R.string.test_evaluate));
		but1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				evaluate();
			}
		});

		startCountDownVar(timePerPage);
	}

	protected void resetVariables() {
		for (int i = 0; i < isItemSelected.length; i++) {
			isItemSelected[i] = false;
		}

		setNumberOfRightSelectedImages(0);
		setNumberOfWrongSelectedImages(0);
		setDuration(timePerPage);
	}

	protected void initMain() {
		AssignedImage = new Vector<Integer>();
		b = new Bundle();
		seznamMereni = new Data();
		generateAssignedImages();
		resetVariables();
	}

	protected void initialize() {
		generateAssignedImages();
		resetVariables();
		generateArrayOfImages();
	}

	private void applySettings() {
		String orientation = PreferenceManager
				.getDefaultSharedPreferences(this).getString(
						"bm_set_orientation", "0");

		if (orientation.equals("1")) { // 1 for portrait, 0 for landscape
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}

		int preferencesCellSize;
		
		if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_cell_size","75").equals("")){
			preferencesCellSize = this.getResources().getInteger(R.integer.test_default_cell_size);
		}else{
			preferencesCellSize = Integer.parseInt(PreferenceManager
					.getDefaultSharedPreferences(this).getString(
							"bm_cell_size",
							Integer.toString(this.getResources().getInteger(
									R.integer.test_default_cell_size))));
		}

		DisplayMetrics display = this.getResources().getDisplayMetrics();

		if (preferencesCellSize == 0) {
			int colNum = (int) ((display.widthPixels) / (cellSize));
			gridview.setNumColumns(colNum);
		} else if (preferencesCellSize > 0) {
			int colNum = (int) ((display.widthPixels) / preferencesCellSize);
			cellSize = preferencesCellSize;
			gridview.setNumColumns(colNum);
		}
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(R.string.test_dialog_interrupt_header);
		builder.setMessage(R.string.test_dialog_interrupt_text);

		builder.setPositiveButton(
				R.string.test_dialog_interrupt_positive_button,

				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						if (timer != null) {
							timer.cancel();
						}
						WorkoutActivity.super.onBackPressed();
						dialog.dismiss();
					}

				});

		builder.setNegativeButton(
				R.string.test_dialog_interrupt_negative_button,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	protected void onStop() {
		if (timer != null) {
			timer.cancel();
		}
		super.onStop();
	}

	private void evaluate() {
		timer.cancel();
		Intent intent = new Intent(getBaseContext(),
				ShowWorkoutResultsActivity.class);
		Data dataPerAllPages = new Data();

		int[] datarow = { getNumberOfRightImages(), getNumberOfWrongImages(),
				getNumberOfRightSelectedImages(),
				getNumberOfWrongSelectedImages(), getDuration(),
				getTestIteration() };
		dataPerAllPages.add(datarow);
		b.putSerializable("dataPerAllPages", dataPerAllPages);
		intent.putExtras(b);
		startActivityForResult(intent, 0);
		finish();
	}

	protected void startCountDownVar(int time) {
		if (timer == null) {
			timer = new CountDownTimer(time, 1000) {
				public void onTick(long millisUntilFinished) {
					countDownTextView.setText((millisUntilFinished / 1000)
							+ " s");
					setDuration((int) millisUntilFinished);
				}

				public void onFinish() {
					evaluate();
				}
			}.start();
		}
	}

	protected void generateAssignedImages() {
		AssignedImage.clear();
		int[] a = Preferences.getImages();
		for (int i = 0; i < a.length; i++) {
			AssignedImage.add(a[i]);
		}

		Collections.shuffle(AssignedImage);
	}

	protected Integer getRandomImage() {
		Vector<Integer> randomImages = new Vector<Integer>();
		int[] a = Preferences.getImages();
		for (int i = 0; i < a.length; i++) {
			randomImages.add(a[i]);
		}
		Collections.shuffle(randomImages);

		return (Integer) randomImages.get(new Random().nextInt(8));
	}

	private void generateArrayOfImages() {
		setNumberOfRightImages(0);
		setNumberOfWrongImages(0);

		for (int i = 0; i < mThumbIds.length; i++) {
			Integer prom = getRandomImage();

			if (prom.equals(first) || prom.equals(second) || prom.equals(third)) {
				setNumberOfRightImages(getNumberOfRightImages() + 1);
			} else {
				setNumberOfWrongImages(getNumberOfWrongImages() + 1);
			}

			mThumbIds[i] = prom;
		}
	}

	// ------------------------------------------------------
	// Getters and setters
	// ------------------------------------------------------

	/**
	 * @param numberOfWrongSelectedImages
	 *            the pocetspatnych to set
	 */
	private void setNumberOfWrongImages(int numberOfWrongSelectedImages) {
		this.wrongCount = numberOfWrongSelectedImages;
	}

	/**
	 * @return the wrongCount
	 */
	private int getNumberOfWrongImages() {
		return wrongCount;
	}

	/**
	 * @param rightCount
	 *            the rightCount to set
	 */
	private void setNumberOfRightImages(int rightCount) {
		this.rightCount = rightCount;
	}

	/**
	 * @return the rightCount
	 */
	private int getNumberOfRightImages() {
		return rightCount;
	}

	/**
	 * @param rightSelectedCount
	 *            the rightSelectedCount to set
	 */
	private void setNumberOfRightSelectedImages(int rightSelectedCount) {
		this.rightSelectedCount = rightSelectedCount;
	}

	/**
	 * @return the rightSelectedCount
	 */
	private int getNumberOfRightSelectedImages() {
		return rightSelectedCount;
	}

	/**
	 * @param pocetspatnezvolenych
	 *            the pocetspatnezvolenych to set
	 */
	private void setNumberOfWrongSelectedImages(int pocetspatnezvolenych) {
		this.wrongSelectedCount = pocetspatnezvolenych;
	}

	/**
	 * @return the pocetspatnezvolenych
	 */
	private int getNumberOfWrongSelectedImages() {
		return wrongSelectedCount;
	}

	/**
	 * @param testDuration
	 *            the testDuration to set
	 */
	private void setDuration(int testDuration) {
		this.testDuration = testDuration;
	}

	/**
	 * @return the testDuration
	 */
	private int getDuration() {
		return testDuration;
	}

	/**
	 * @return the testIteration
	 */
	private int getTestIteration() {
		return testIteration;
	}
}