package eu.slachta.bm.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjoe64.graphview.CustomLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

import eu.slachta.bm.R;

public class ShowGraphActivity extends Activity {
	private LineGraphView graphView;
	public static int[] WRONGSETCOUNT;
	public static int[] RIGHTSETCOUNT;
	public static int[] WRONGALLCOUNT;
	public static int[] RIGHTALLCOUNT;
	public static int[] TIMESPENT;
	private int xAxisMax;
	private TextView tv;
	private int type;
	private static int timePerPage;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.activity_show_graph);
		LinearLayout l = (LinearLayout) findViewById(R.id.graphLayout);
		tv = (TextView) findViewById(R.id.graphTextView);
		type = getIntent().getIntExtra("type", 0);
        GraphViewSeries series;
        graphView = new LineGraphView(this, "");

        int yAxisMax = 0;
        int xAxisMax = 0;

        if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_number_of_images_per_page","85").equals("")) {
            yAxisMax = 85;
        }else{
            yAxisMax = Integer.parseInt(PreferenceManager
                    .getDefaultSharedPreferences(this).getString(
                            "bm_number_of_images_per_page",
                            Integer.toString(this.getResources().getInteger(R.integer.test_number_of_images_per_page))));
        }

        if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_number_of_pages","30").equals("")) {
            xAxisMax = 30;
        }else{
            xAxisMax = Integer.parseInt(PreferenceManager
                    .getDefaultSharedPreferences(this).getString(
                            "bm_number_of_pages",
                            Integer.toString(this.getResources().getInteger(R.integer.test_number_of_pages))));
        }

		switch (this.type) {
		case 0:
			TIMESPENT = getIntent().getIntArrayExtra("timeSpent");
			WRONGSETCOUNT = getIntent().getIntArrayExtra("wrongSet");
			RIGHTSETCOUNT = getIntent().getIntArrayExtra("rightSet");

            series = new GraphViewSeries(getResources().getString(R.string.graph_legend_time),new GraphViewSeries.GraphViewSeriesStyle(Color.BLUE, 3), getGraphData(TIMESPENT));
            graphView.addSeries(series);

            series = new GraphViewSeries(getResources().getString(R.string.graph_legend_wrong),new GraphViewSeries.GraphViewSeriesStyle(Color.RED, 3), getGraphData(WRONGSETCOUNT));
            graphView.addSeries(series);

            series = new GraphViewSeries(getResources().getString(R.string.graph_legend_right),new GraphViewSeries.GraphViewSeriesStyle(Color.GREEN, 3), getGraphData(RIGHTSETCOUNT));
            graphView.addSeries(series);

            graphView.setManualYAxisBounds(100,0);

			tv.setText(Html.fromHtml(getResources().getString(R.string.graph_cumulative_description)));

            setTitle(getResources().getString(R.string.graph_activity_title_cumulative));

			break;
		case 1:
			WRONGSETCOUNT = getIntent().getIntArrayExtra("wrongSet");
			WRONGALLCOUNT = getIntent().getIntArrayExtra("wrongAll");
            //TODO: axis titles are not displayed, not sure what method displays them
            //TODO: fix the number of labels on axises

            series = new GraphViewSeries(getResources().getString(R.string.graph_legend_wrong),new GraphViewSeries.GraphViewSeriesStyle(Color.BLUE, 3), getGraphData(WRONGSETCOUNT));
            graphView.addSeries(series);

            series = new GraphViewSeries(getResources().getString(R.string.graph_legend_wrong_max),new GraphViewSeries.GraphViewSeriesStyle(Color.BLACK, 3), getGraphData(WRONGALLCOUNT));
            graphView.addSeries(series);

            graphView.setManualYAxisBounds(yAxisMax,0);

			tv.setText(Html.fromHtml(getResources().getString(R.string.graph_failure_description)));

            setTitle(getResources().getString(R.string.graph_activity_title_failure));

			break;
		case 2:
			RIGHTSETCOUNT = getIntent().getIntArrayExtra("rightSet");
			RIGHTALLCOUNT = getIntent().getIntArrayExtra("rightAll");

            series = new GraphViewSeries(getResources().getString(R.string.graph_legend_right),new GraphViewSeries.GraphViewSeriesStyle(Color.BLUE, 3), getGraphData(RIGHTSETCOUNT));
            graphView.addSeries(series);

            series = new GraphViewSeries(getResources().getString(R.string.graph_legend_right_max),new GraphViewSeries.GraphViewSeriesStyle(Color.BLACK, 3), getGraphData(RIGHTALLCOUNT));
            graphView.addSeries(series);

            graphView.setManualYAxisBounds(yAxisMax,0);

			tv.setText(Html.fromHtml(getResources().getString(R.string.graph_success_description)));

            setTitle(getResources().getString(R.string.graph_activity_title_fruitfulness));

			break;
		case 3:
			TIMESPENT = getIntent().getIntArrayExtra("timeSpent");
			timePerPage = getIntent().getIntExtra("timePerPage",50000);

            series = new GraphViewSeries(getResources().getString(R.string.graph_legend_time),new GraphViewSeries.GraphViewSeriesStyle(Color.BLUE, 3), getTimeGraphData(TIMESPENT, timePerPage));
            graphView.addSeries(series);

            series = new GraphViewSeries(getResources().getString(R.string.graph_legend_time_max),new GraphViewSeries.GraphViewSeriesStyle(Color.BLACK, 3), getGraphData(timePerPage,TIMESPENT.length));
            graphView.addSeries(series);

            graphView.setManualYAxisBounds(timePerPage,0);

			tv.setText(Html.fromHtml(getResources().getString(R.string.graph_speed_description)));

            setTitle(getResources().getString(R.string.graph_activity_title_speed));

			break;
		default:
			break;
		}

        setGraphStyle(graphView);
		l.addView(graphView);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

    public void setGraphStyle(LineGraphView graphView) {
        //graphView.setDrawDataPoints(true);
        graphView.setShowLegend(true);
        graphView.setLegendAlign(GraphView.LegendAlign.BOTTOM);
        graphView.getGraphViewStyle().setLegendBorder(20);
        graphView.getGraphViewStyle().setLegendSpacing(30);
        graphView.getGraphViewStyle().setLegendWidth(350);
        graphView.getGraphViewStyle().setGridColor(Color.DKGRAY);
        graphView.getGraphViewStyle().setTextSize(tv.getTextSize());
        graphView.setScalable(false);
        graphView.setCustomLabelFormatter(new CustomLabelFormatter() {

            @Override
            public String formatLabel(double value, boolean isValueX) {
                if(!isValueX) {
                    return Integer.toString((int) value);
                } else {
                    return null;
                }
                //return value+"";
            }
        });
    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
	   switch (item.getItemId()) 
	   {
	     case android.R.id.home:
	        onBackPressed();
	        return true;
	     default:
	        return super.onOptionsItemSelected(item);
	   }
	}

    private GraphView.GraphViewData[] getGraphData(int[] arr){
        GraphView.GraphViewData[] graphData = new GraphView.GraphViewData[arr.length];
        for (int k = 0; k < arr.length; k++) {
			graphData[k]= new GraphView.GraphViewData(k, arr[k]);
		}

        return graphData;
    }

    private GraphView.GraphViewData[] getGraphData(int arr, int numberOfRepetitions){
        GraphView.GraphViewData[] graphData = new GraphView.GraphViewData[numberOfRepetitions];
        for (int k = 0; k < numberOfRepetitions; k++) {
            graphData[k]= new GraphView.GraphViewData(k, arr);
        }
        return graphData;
    }

    private GraphView.GraphViewData[] getTimeGraphData(int[] arr, int max){
        GraphView.GraphViewData[] graphData = new GraphView.GraphViewData[arr.length];
        for (int k = 0; k < arr.length; k++) {
            graphData[k]= new GraphView.GraphViewData(k, max-arr[k]);
        }
        return graphData;
    }

}