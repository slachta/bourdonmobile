package eu.slachta.bm.activity;

import java.util.Collections;
import java.util.Random;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import eu.slachta.bm.util.Preferences;
import eu.slachta.bm.R;
import eu.slachta.bm.adapter.ImageAdapter;
import eu.slachta.bm.model.Data;
import eu.slachta.bm.model.FormAndDeviceData;

public class FullTestActivity extends Activity {

	// ----
	// Test variables, objects and arrays
	// ----

	private Vector<Integer> AssignedImage;
	private int ARRAY_SIZE;
	private Integer[] mThumbIds;
	private boolean[] isItemSelected;
	private CountDownTimer timer;
	protected int first;
	protected int firstId;
	protected int second;
	protected int secondId;
	protected int third;
	protected int thirdId;

	// ----
	// GUI components
	// ----

	private TextView pageCounterTextView;
	private TextView countDownTextView;
	private Button but1;
	private Button but2;
	private ImageView im1;
	private ImageView im2;
	private ImageView im3;
	protected GridView gridview;

	// ----
	// Test data variables (per page)
	// ----

	private int rightCount = 0;
	private int wrongCount = 0;
	private int rightSelectedCount = 0;
	private int wrongSelectedCount = 0;
	private int testDuration = 0;
	private int testIteration = 1;

	private int measurementCount;
	private int timePerPage;

	// ----
	// Cumulative variables
	// ----

	private Bundle b;
	private Data dataPerAllPages;
	private FormAndDeviceData formAndDeviceData;

	// ----
	// GUI params
	// ----

	private int orientation;
	private int preferencesCellSize;

	// ----
	// On Create
	// ----

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getActionBar().hide();

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full_test);
		
		// initialize preference variables
		//----------------------------------------------------------------------------------
		
		formAndDeviceData = (FormAndDeviceData) getIntent()
				.getSerializableExtra("formAndDeviceData");

		if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_number_of_images_per_page","30").equals("")){
			this.ARRAY_SIZE = this.getResources().getInteger(R.integer.test_number_of_images_per_page);
		}else{
			this.ARRAY_SIZE = Integer.parseInt(PreferenceManager
					.getDefaultSharedPreferences(this).getString(
							"bm_number_of_images_per_page",
							Integer.toString(this.getResources().getInteger(
									R.integer.test_number_of_images_per_page))));
		}
		
		formAndDeviceData.setNumberOfImagesPerPage(ARRAY_SIZE);
		mThumbIds = new Integer[ARRAY_SIZE];
		isItemSelected = new boolean[ARRAY_SIZE];
		
		if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_number_of_pages","30").equals("")){
			this.measurementCount = this.getResources().getInteger(R.integer.test_number_of_pages);
		}else{
			this.measurementCount = Integer.parseInt(PreferenceManager
					.getDefaultSharedPreferences(this).getString(
							"bm_number_of_pages",
							Integer.toString(this.getResources().getInteger(
									R.integer.test_number_of_pages))));
		}
		
		formAndDeviceData.setNumberOfPages(measurementCount);
		
		if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_time_per_page","50000").equals("")){
			this.timePerPage = this.getResources().getInteger(R.integer.test_time_per_page);
		}else{
			this.timePerPage = Integer.parseInt(PreferenceManager
					.getDefaultSharedPreferences(this).getString(
							"bm_time_per_page",
							Integer.toString(this.getResources().getInteger(
									R.integer.test_time_per_page))));
		}

		formAndDeviceData.setTimePerPage(timePerPage);
		
		if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_set_orientation","0").equals("")){ // 0 for landscape
			this.orientation = this.getResources().getInteger(R.integer.test_default_orientation);
		}else{
			this.orientation = Integer.parseInt(PreferenceManager
					.getDefaultSharedPreferences(this).getString(
							"bm_set_orientation",
							Integer.toString(this.getResources().getInteger(
									R.integer.test_default_orientation))));
		}

		formAndDeviceData.setOrientation(this.orientation);

		if (this.orientation == 1) { // 1 for portrait, 0 for landscape
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
		
		// initialize internal variables
		//----------------------------------------------------------------------------------

		if (dataPerAllPages == null) {
			initMain();
		}

		// prepare views for full test
		//----------------------------------------------------------------------------------
		
		setTitle("Test");
		TextView tv1 = (TextView) findViewById(R.id.full_test_preview_textview_description);
		tv1.setText(Html.fromHtml(getResources()
				.getString(R.string.test_header)));

		im1 = (ImageView) findViewById(R.id.full_test_imageprev_1_imageview);
		first = AssignedImage.elementAt(1);
		firstId = Preferences.getProperImageId(first);
		formAndDeviceData.setRefImg1Id(firstId);
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), first);
		im1.setImageBitmap(bmp);
		im1.invalidate();

		im2 = (ImageView) findViewById(R.id.full_test_imageprev_2_imageview);
		second = AssignedImage.elementAt(2);
		secondId = Preferences.getProperImageId(second);
		formAndDeviceData.setRefImg2Id(secondId);
		bmp = BitmapFactory.decodeResource(getResources(), second);
		im2.setImageBitmap(bmp);
		im2.invalidate();

		im3 = (ImageView) findViewById(R.id.full_test_imageprev_3_imageview);
		third = AssignedImage.elementAt(3);
		thirdId = Preferences.getProperImageId(third);
		formAndDeviceData.setRefImg3Id(thirdId);
		bmp = BitmapFactory.decodeResource(getResources(), third);
		im3.setImageBitmap(bmp);
		im3.invalidate();

		gridview = (GridView) findViewById(R.id.full_test_gridview);

		generateArrayOfImages();
		
		// set cell size and number of columns
		//----------------------------------------------------------------------------------
		
		if(PreferenceManager.getDefaultSharedPreferences(this).getString("bm_cell_size",Integer.toString(this.getResources().getInteger(R.integer.test_default_cell_size))).equals("")){
			this.preferencesCellSize = this.getResources().getInteger(R.integer.test_default_cell_size);
		}else{
			this.preferencesCellSize = Integer.parseInt(PreferenceManager
					.getDefaultSharedPreferences(this).getString(
							"bm_cell_size",
							Integer.toString(this.getResources().getInteger(
									R.integer.test_default_cell_size))));
		}
		
		formAndDeviceData.setCellSize(this.preferencesCellSize);

		DisplayMetrics display = this.getResources().getDisplayMetrics();

		if (this.preferencesCellSize == 0) {
            this.preferencesCellSize = getResources().getInteger(R.integer.test_default_cell_size);
		} else if(preferencesCellSize > 0) {
			int colNum = (int) ((display.widthPixels) / this.preferencesCellSize);
			gridview.setNumColumns(colNum);
		}
		
		// gridview params
		//----------------------------------------------------------------------------------

		gridview.setAdapter(new ImageAdapter(this, mThumbIds, this.preferencesCellSize));
		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				int viewId = v.getId();
				if ((viewId == firstId) || (viewId == secondId) || (viewId == thirdId)) {
					if (!isItemSelected[position]) {
						mThumbIds[position] = Preferences
								.getAssociatedImage(Preferences
										.getImageById(viewId));
						setNumberOfRightSelectedImages(getNumberOfRightSelectedImages() + 1);
					}
				} else {
					if (!isItemSelected[position]) {
						mThumbIds[position] = Preferences
								.getAssociatedImage(Preferences
										.getImageById(viewId));
						setNumberOfWrongSelectedImages(getNumberOfWrongSelectedImages() + 1);
					}
				}
				gridview.setAdapter(new ImageAdapter(parent.getContext(),
						mThumbIds, preferencesCellSize));
				gridview.setSelection(position);
				isItemSelected[position] = true;
			}
		});

		countDownTextView = (TextView) findViewById(R.id.full_test_counter);

		pageCounterTextView = (TextView) findViewById(R.id.full_test_page_counter);
		pageCounterTextView.setText(Html.fromHtml(this.testIteration + " / "
				+ this.measurementCount));

		// prepare buttons and set listeners for them
		//----------------------------------------------------------------------------------
		
		but1 = (Button) findViewById(R.id.full_test_button_interrupt_test);
		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);

		if (tabletSize) {
			but1.setText(getResources().getString(R.string.test_next_images));
		} else {
			Drawable myIcon = this.getResources().getDrawable(R.drawable.ic_action_play);
			but1.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
					myIcon);
		}

		but1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				evaluate();
			}
		});

		but2 = (Button) findViewById(R.id.full_test_button_skip);
		but2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				evaluateOnSkip();
			}
		});

		startCountDownVar(this.timePerPage);
	}

	protected void resetVariables() {
		for (int i = 0; i < isItemSelected.length; i++) {
			isItemSelected[i] = false;
		}

		setNumberOfRightSelectedImages(0);
		setNumberOfWrongSelectedImages(0);
		setDuration(timePerPage);
	}

	protected void initMain() {
		AssignedImage = new Vector<Integer>();
		b = new Bundle();
		dataPerAllPages = new Data();
		generateAssignedImages();
		resetVariables();
	}

	protected void initialize() {
		generateAssignedImages();
		resetVariables();
		generateArrayOfImages();
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(R.string.test_dialog_interrupt_header);
		builder.setMessage(R.string.test_dialog_interrupt_text);

		builder.setPositiveButton(
				R.string.test_dialog_interrupt_positive_button,

				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						if (timer != null) {
							timer.cancel();
						}
						FullTestActivity.super.onBackPressed();
						dialog.dismiss();
					}

				});

		builder.setNegativeButton(
				R.string.test_dialog_interrupt_negative_button,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (timer != null) {
			timer.cancel();
		}
	}

	/**
	 * Evaluates a test and starts AdvancedResults activity. If the test
	 * iteration equals to page measurement count, it passes the form data and
	 * the pages data to AdvancedResults activity.
	 * 
	 */

	private void evaluate() {
		timer.cancel();
		timer = null;
		int[] datarow = { getNumberOfRightImages(), getNumberOfWrongImages(),
				getNumberOfRightSelectedImages(),
				getNumberOfWrongSelectedImages(), getDuration(),
				getTestIteration() };

		dataPerAllPages.add(this.testIteration - 1, datarow);
		initialize();
		if (this.testIteration == this.measurementCount) {
			b.putSerializable("dataPerAllPages", dataPerAllPages);

			storeDeviceData();

			Intent intent = new Intent(getBaseContext(),
					ShowAdvancedResultsActivity.class);
			intent.putExtras(b);
			intent.putExtra("state", 0);
			intent.putExtra("formAndDeviceData", formAndDeviceData);
			intent.putExtra("isInterrupted", false);
			startActivityForResult(intent, 0);
			finish();
		} else {
			if (this.testIteration == (this.measurementCount - 1)) {
				boolean tabletSize = getResources().getBoolean(R.bool.isTablet);

				if (tabletSize) {
					but1.setText(getResources().getString(R.string.test_evaluate));
				} else {
					Drawable myIcon = this.getResources().getDrawable(
							R.drawable.ic_action_stop);
					but1.setCompoundDrawablesWithIntrinsicBounds(null, null,
							null, myIcon);
				}
			}

			gridview.setAdapter(new ImageAdapter(this.getApplicationContext(), mThumbIds, this.preferencesCellSize));
			this.testIteration++;
			pageCounterTextView.setText(Html.fromHtml(this.testIteration
					+ " / " + this.measurementCount));

			startCountDownVar(timePerPage);
		}
	}

	/**
	 * Evaluates a test and starts AdvancedResults activity without performing
	 * remaining screens. The rest of the data is filled by zeros.
	 * 
	 */

	private void evaluateOnSkip() {
		timer.cancel();
		int[] datarow = { getNumberOfRightImages(), getNumberOfWrongImages(),
				getNumberOfRightSelectedImages(),
				getNumberOfWrongSelectedImages(), getDuration(),
				getTestIteration() };

		dataPerAllPages.add(this.testIteration - 1, datarow);
        but1.setEnabled(false);

		initialize();

		storeDeviceData();

		b.putSerializable("dataPerAllPages", dataPerAllPages);
		Intent intent = new Intent(getBaseContext(),
				ShowAdvancedResultsActivity.class);
		intent.putExtras(b);
		intent.putExtra("state", 0);
		intent.putExtra("formAndDeviceData", formAndDeviceData);
		intent.putExtra("isInterrupted", true);
		startActivityForResult(intent, 0);
		finish();
	}

	private void storeDeviceData() {
		formAndDeviceData.setResolution(getResolution());
		formAndDeviceData.setDpi(getDensityDpi());
		formAndDeviceData.setOrientation(getOrientation());
		formAndDeviceData.setCellSize(getCellSize());
	}

	protected void startCountDownVar(int time) {
		if (timer == null) {
			timer = new CountDownTimer(time, 1000) {
				public void onTick(long millisUntilFinished) {
					countDownTextView.setText((millisUntilFinished / 1000)
							+ " s");
					setDuration((int) millisUntilFinished);
				}

				public void onFinish() {
					evaluate();
				}
			}.start();
		}
	}

	protected void generateAssignedImages() {
		AssignedImage.clear();
		int[] a = Preferences.getImages();
		for (int i = 0; i < a.length; i++) {
			AssignedImage.add(a[i]);
		}

		Collections.shuffle(AssignedImage);
	}

	protected Integer getRandomImage() {
		Vector<Integer> randomImages = new Vector<Integer>();
		int[] a = Preferences.getImages();
		for (int i = 0; i < a.length; i++) {
			randomImages.add(a[i]);
		}
		Collections.shuffle(randomImages);

		return (Integer) randomImages.get(new Random().nextInt(8));
	}

	private void generateArrayOfImages() {
		setNumberOfRightImages(0);
		setNumberOfWrongImages(0);

		for (int i = 0; i < mThumbIds.length; i++) {
			Integer prom = getRandomImage();

			if (prom.equals(first) || prom.equals(second) || prom.equals(third)) {
				setNumberOfRightImages(getNumberOfRightImages() + 1);
			} else {
				setNumberOfWrongImages(getNumberOfWrongImages() + 1);
			}

			mThumbIds[i] = prom;
		}
	}

	// ------------------------------------------------------
	// Getters and setters
	// ------------------------------------------------------

	/**
	 * @param numberOfWrongSelectedImages
	 *            the pocetspatnych to set
	 */
	private void setNumberOfWrongImages(int numberOfWrongSelectedImages) {
		this.wrongCount = numberOfWrongSelectedImages;
	}

	/**
	 * @return the wrongCount
	 */
	private int getNumberOfWrongImages() {
		return wrongCount;
	}

	/**
	 * @param rightCount
	 *            the rightCount to set
	 */
	private void setNumberOfRightImages(int rightCount) {
		this.rightCount = rightCount;
	}

	/**
	 * @return the rightCount
	 */
	private int getNumberOfRightImages() {
		return rightCount;
	}

	/**
	 * @param rightSelectedCount
	 *            the rightSelectedCount to set
	 */
	private void setNumberOfRightSelectedImages(int rightSelectedCount) {
		this.rightSelectedCount = rightSelectedCount;
	}

	/**
	 * @return the rightSelectedCount
	 */
	private int getNumberOfRightSelectedImages() {
		return rightSelectedCount;
	}

	/**
	 * @param pocetspatnezvolenych
	 *            the pocetspatnezvolenych to set
	 */
	private void setNumberOfWrongSelectedImages(int pocetspatnezvolenych) {
		this.wrongSelectedCount = pocetspatnezvolenych;
	}

	/**
	 * @return the pocetspatnezvolenych
	 */
	private int getNumberOfWrongSelectedImages() {
		return wrongSelectedCount;
	}

	/**
	 * @param testDuration
	 *            the testDuration to set
	 */
	private void setDuration(int testDuration) {
		this.testDuration = testDuration;
	}

	/**
	 * @return the testDuration
	 */
	private int getDuration() {
		return testDuration;
	}

	/**
	 * @return the testIteration
	 */
	private int getTestIteration() {
		return testIteration;
	}

	/**
	 * Returns density of the display.
	 * 
	 * @return display DPI
	 */

	// private Float getDensity(){
	// DisplayMetrics displaymetrics = new DisplayMetrics();
	// getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
	//
	// return displaymetrics.density;
	// }

	/**
	 * Returns density DPI of the display.
	 * 
	 * @return display DPI
	 */

	private int getDensityDpi() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		return displaymetrics.densityDpi;
	}

	/**
	 * Returns resolution of the device.
	 * 
	 * @return display resolution
	 */

	private String getResolution() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;

		if (height > width)
			return height + "," + width;
		else
			return width + "," + height;
	}

	/**
	 * Returns resolution of the device.
	 * 
	 * @return display orientation
	 */

	private int getOrientation() {
		return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(
				this).getString("bm_set_orientation", "0"));
	}

	/**
	 * Returns resolution of the device.
	 * 
	 * @return cell size
	 */

	private int getCellSize() {
		return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(
				this).getString("bm_cell_size", "0"));
	}
}