package eu.slachta.bm.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import eu.slachta.bm.R;
import eu.slachta.bm.device.Functions;

public class QRCodeReaderActivity extends Activity implements OnClickListener {
    private Button scanBtn;
    private TextView content, header;
    private String uri = "";
    private String apiKey = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_qr_code);
        scanBtn = (Button) findViewById(R.id.qr_code_do_read);
        header = (TextView) findViewById(R.id.qr_code_reader_header);
        content = (TextView) findViewById(R.id.qr_code_reader_content);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        scanBtn.setOnClickListener(this);

        header.setText(Html.fromHtml("<b><big>" + getResources().getString(R.string.qr_code_header) + "</big></b>"));

        this.apiKey = PreferenceManager
                .getDefaultSharedPreferences(this).getString(
                        "app_settings_api_key", "");

        this.uri = PreferenceManager
                .getDefaultSharedPreferences(this).getString(
                        "app_settings_server_uri", "");

        fillTextViewWithURIAndApiKey();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(
                requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            if (scanContent != null) {
                String[] kvPairs = new String[2];
                if (scanContent.contains(";")) {
                    kvPairs = scanContent.split(";");
                } else {
                    kvPairs[0]=scanContent;
                    kvPairs[1]="";
                }

                boolean isApiValid = false;

                for (String kvPair : kvPairs) {
                    if(kvPair.contains("=")) {
                        String[] kv = kvPair.split("=");
                        String key = kv[0];
                        String value = kv[1];
                        if (key.equals("uri")) {
                            this.setUri(value);
                        } else if (key.equals("apikey")) {
                            isApiValid = Functions.isApiKeyValid(value);
                            if (isApiValid) {
                                this.setApiKey(value);
                            }
                        }
                    }
                }
                if (isApiValid) {
                    Editor editor = PreferenceManager
                            .getDefaultSharedPreferences(
                                    getApplicationContext()).edit();
                    editor.putString("app_settings_api_key", this.apiKey);
                    editor.commit();

                    this.apiKey = PreferenceManager
                            .getDefaultSharedPreferences(this).getString(
                                    "app_settings_api_key", "");
                }

                if ((this.uri != null) && !(uri.equals(""))) {
                    Editor editor = PreferenceManager
                            .getDefaultSharedPreferences(
                                    getApplicationContext()).edit();
                    editor.putString("app_settings_server_uri", this.uri);
                    editor.commit();

                    this.uri = PreferenceManager
                            .getDefaultSharedPreferences(this).getString(
                                    "app_settings_server_uri", "");
                }
            }

            fillTextViewWithURIAndApiKey();

        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void fillTextViewWithURIAndApiKey() {
        content.setText(
                Html.fromHtml(
                        getResources().getString(R.string.qr_code_header_uri) + ": " + this.uri + "<br>"
                                + getResources().getString(R.string.qr_code_header_apikey) + ": " + this.apiKey
                )
        );
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.qr_code_do_read) {
            if (Functions.isPackageInstalled("com.google.zxing.client.android",
                    getApplicationContext())) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(this);
                scanIntegrator.initiateScan();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle(R.string.qr_code_zxing_not_installed_title);
                builder.setMessage(R.string.qr_code_zxing_not_installed_content);

                builder.setPositiveButton(
                        R.string.qr_code_zxing_not_installed_positive_response,

                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Intent goToMarket = new Intent(
                                        Intent.ACTION_VIEW).setData(Uri
                                        .parse("market://details?id=com.google.zxing.client.android"));
                                startActivity(goToMarket);
                                dialog.dismiss();
                            }

                        });

                builder.setNegativeButton(
                        R.string.qr_code_zxing_not_installed_negative_response,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * @return the uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * @param uri the uri to set
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * @return the apiKey
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * @param apiKey the apiKey to set
     */
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
