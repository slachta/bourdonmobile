package eu.slachta.bm.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import eu.slachta.bm.R;
import eu.slachta.bm.model.BourdonMobileTestInformation;

@SuppressLint("UseSparseArrays")
public class ResultsAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private ArrayList<BourdonMobileTestInformation> allResults;
	private HashMap<Integer, Boolean> mSelection;

	public class ViewHolder {
		TextView v1;
		TextView v2;
		TextView v3;
	}

	public ResultsAdapter(Context context, ArrayList<BourdonMobileTestInformation> objects) {
		inflater = LayoutInflater.from(context);
		this.allResults = objects;
		mSelection = new HashMap<Integer, Boolean>();
	}
	
	public void refreshData(ArrayList<BourdonMobileTestInformation> allResults){
		this.allResults=allResults;
		notifyDataSetChanged();
	}
	
    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

	public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.fragment_results_item, null);
			holder.v1 = (TextView) convertView.findViewById(R.id.results_textview_id);
			holder.v2 = (TextView) convertView.findViewById(R.id.results_textview_content);
			holder.v3 = (TextView) convertView.findViewById(R.id.results_textview_description);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.v1.setText(Long.toString(allResults.get(position).getTestId()));
		holder.v2.setText(parent.getResources().getString(R.string.results_test_date)+" "+allResults.get(position).getDate());
		holder.v3.setText(parent.getResources().getString(R.string.results_note)+": "+allResults.get(position).getNote());
		
		return convertView;
	}

	@Override
	public int getCount() {
		return this.allResults.size();
	}

	@Override
	public BourdonMobileTestInformation getItem(int position) {
		return allResults.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
    }
}
