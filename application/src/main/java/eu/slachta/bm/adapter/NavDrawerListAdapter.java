package eu.slachta.bm.adapter;

import eu.slachta.bm.R;
import eu.slachta.bm.model.NavDrawerItem;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavDrawerListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;

	public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// if item is section, select drawer_list_section_item layout to
		// inflate, otherwise choose drawer_list_item
		if (navDrawerItems.get(position).isSection()) {
			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.drawer_list_section_item, null);
			TextView title = (TextView) convertView.findViewById(R.id.section_title);

			title.setClickable(false);
			title.setLongClickable(false);
			title.setFocusable(false);
			title.setText(navDrawerItems.get(position).getTitle());
		} else {
			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.drawer_list_item, null);

			ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
			TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
			TextView txtCount = (TextView) convertView.findViewById(R.id.counter);
			NavDrawerItem ndi = (NavDrawerItem) getItem(position);

			imgIcon.setImageResource(ndi.getIcon());
			txtTitle.setText(ndi.getTitle());

			// displaying count
			// check whether it set visible or not
			if (ndi.getCounterVisibility()) {
				txtCount.setText(ndi.getCount());
			} else {
				// hide the counter view
				txtCount.setVisibility(View.GONE);
			}
		}

		return convertView;
	}

}
