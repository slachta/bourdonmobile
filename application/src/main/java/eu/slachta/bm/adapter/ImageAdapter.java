package eu.slachta.bm.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import eu.slachta.bm.util.Preferences;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;
	private Integer[] mThumbIds;
	private int cellSize;

	public class ViewHolder {
		ImageView imageView;
	}
	
	public ImageAdapter(Context c, Integer[] mThumbIds, int cellSize) {
		this.cellSize = cellSize;
		this.mThumbIds = mThumbIds;
		mContext = c;
	}

	public int getCount() {
		return mThumbIds.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		if (convertView == null) {
			holder.imageView = new ImageView(mContext);
            holder.imageView.setLayoutParams(new GridView.LayoutParams(cellSize, cellSize));
			holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			holder.imageView.setPadding(10, 10, 10, 10);
		} else {
			holder.imageView = (ImageView) convertView;
		}
		holder.imageView.setId(Preferences.getProperImageId(mThumbIds[position]));
		holder.imageView.setImageResource(mThumbIds[position]);
		return holder.imageView;
	}
}