package eu.slachta.bm.model;

import java.util.ArrayList;

import eu.slachta.bm.db.DatabaseHandler;

public class BourdonMobileTest {

	private int dbVersion = DatabaseHandler.DB_VERSION;
	private BourdonMobileTestInformation bourdonTest;
	private ArrayList<BourdonMobileTestPageData> bourdonTestData;

	public BourdonMobileTest(int dbVersion, BourdonMobileTestInformation test, ArrayList<BourdonMobileTestPageData> testData) {
		this.setDbVersion(dbVersion);
		this.setBourdonMobileTestInformation(test);
		this.setBourdonMobileTestData(testData);
	}

	/**
	 * @return the dbVersion
	 */
	public int getDbVersion() {
		return dbVersion;
	}

	/**
	 * @param dbVersion the dbVersion to set
	 */
	public void setDbVersion(int dbVersion) {
		this.dbVersion = dbVersion;
	}
	
	/**
	 * @return the bourdonTest
	 */
	public BourdonMobileTestInformation getBourdonMobileTestInformation() {
		return bourdonTest;
	}

	/**
	 * @param bourdonTest the bourdonTest to set
	 */
	public void setBourdonMobileTestInformation(BourdonMobileTestInformation bourdonTest) {
		this.bourdonTest = bourdonTest;
	}

	/**
	 * @return the bourdonTestData
	 */
	public ArrayList<BourdonMobileTestPageData> getBourdonMobileTestData() {
		return bourdonTestData;
	}

	/**
	 * @param bourdonTestData the bourdonTestData to set
	 */
	public void setBourdonMobileTestData(ArrayList<BourdonMobileTestPageData> bourdonTestData) {
		this.bourdonTestData = bourdonTestData;
	}
}
