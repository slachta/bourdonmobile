package eu.slachta.bm.model;

import java.io.Serializable;
import java.util.Vector;
 
public class Data implements Serializable{
        private static final long serialVersionUID = 663585476779879096L;
        private Vector<int[]> data;
        
        public Data(){
        	data = new Vector<int[]>();
        }
        
        public boolean add(int location, int[] object ){
        	data.add(location, object);
        	return true;
        }
        
        public boolean add(int[] object ){
        	data.add(object);
        	return true;
        }
        
        public int size(){
        	return this.data.size();
        }
        
        public int[] get(int location){
        	return this.data.get(location);
        }

        public Vector<int[]> getData(){
        	return this.data;
        }
        
        public void setData(Vector<int[]> data){
        	this.data=data;
        }
}
