package eu.slachta.bm.model;

import java.io.Serializable;
 
public class FormAndDeviceData implements Serializable{
	private static final long serialVersionUID = 3157962559462178703L;

	private Integer age;
	private Integer sex;
	private Integer educationLevel;
	private String note;
	private String resolution;
	private Integer dpi;
	private Integer orientation;
	private Integer cellSize;
	private String userid;
	private Integer numberOfImagesPerPage;
	private Integer numberOfPages;
	private Integer timePerPage;
	private Integer refImg1Id;
	private Integer refImg2Id;
	private Integer refImg3Id;
	
	public FormAndDeviceData(){
		
	}

	/**
	 * @return the sex
	 */
	public Integer getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * @return the educationLevel
	 */
	public Integer getEducationLevel() {
		return educationLevel;
	}

	/**
	 * @param educationLevel the educationLevel to set
	 */
	public void setEducationLevel(Integer educationLevel) {
		this.educationLevel = educationLevel;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the dpi
	 */
	public Integer getDpi() {
		return dpi;
	}

	/**
	 * @param dpi the dpi to set
	 */
	public void setDpi(Integer dpi) {
		this.dpi = dpi;
	}

	/**
	 * @return the resolution
	 */
	public String getResolution() {
		return resolution;
	}

	/**
	 * @param resolution the resolution to set
	 */
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	/**
	 * @return the orientation
	 */
	public Integer getOrientation() {
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(Integer orientation) {
		this.orientation = orientation;
	}

	/**
	 * @return the cellSize
	 */
	public Integer getCellSize() {
		return cellSize;
	}

	/**
	 * @param cellSize the cellSize to set
	 */
	public void setCellSize(Integer cellSize) {
		this.cellSize = cellSize;
	}

	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/**
	 * @return the numberOfImagesPerPage
	 */
	public Integer getNumberOfImagesPerPage() {
		return numberOfImagesPerPage;
	}

	/**
	 * @param numberOfImagesPerPage the numberOfImagesPerPage to set
	 */
	public void setNumberOfImagesPerPage(Integer numberOfImagesPerPage) {
		this.numberOfImagesPerPage = numberOfImagesPerPage;
	}

	/**
	 * @return the numberOfPages
	 */
	public Integer getNumberOfPages() {
		return numberOfPages;
	}

	/**
	 * @param numberOfPages the numberOfPages to set
	 */
	public void setNumberOfPages(Integer numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	/**
	 * @return the timePerPage
	 */
	public Integer getTimePerPage() {
		return timePerPage;
	}

	/**
	 * @param timePerPage the timePerPage to set
	 */
	public void setTimePerPage(Integer timePerPage) {
		this.timePerPage = timePerPage;
	}

	/**
	 * @return the refImg1Id
	 */
	public Integer getRefImg1Id() {
		return refImg1Id;
	}

	/**
	 * @param refImg1Id the refImg1Id to set
	 */
	public void setRefImg1Id(Integer refImg1Id) {
		this.refImg1Id = refImg1Id;
	}

	/**
	 * @return the refImg2Id
	 */
	public Integer getRefImg2Id() {
		return refImg2Id;
	}

	/**
	 * @param refImg2Id the refImg2Id to set
	 */
	public void setRefImg2Id(Integer refImg2Id) {
		this.refImg2Id = refImg2Id;
	}

	/**
	 * @return the refImg3Id
	 */
	public Integer getRefImg3Id() {
		return refImg3Id;
	}

	/**
	 * @param refImg3Id the refImg3Id to set
	 */
	public void setRefImg3Id(Integer refImg3Id) {
		this.refImg3Id = refImg3Id;
	}
}
