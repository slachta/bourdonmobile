package eu.slachta.bm.model;

public class ListViewKeyValueItem {
	private String key;
	private String value;
	
	public ListViewKeyValueItem(String key, String value){
		this.setKey(key);
		this.setValue(value);
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
