package eu.slachta.bm.model;

import java.text.DateFormat;
import java.util.Date;

public class BourdonMobileTestInformation {
	private long testId;
	private String userId;
	private int imagesPerPage;
	private int timePerPage;
	private int pages;
	private long time;
	private int age;
	private int sex;
	private int educationLevel;
	private String note;
	private String resolution;
	private int dpi;
	private int orientation;
	private int cellSize;
	private int refImg1Id;
	private int refImg2Id;
	private int refImg3Id;
    
	public BourdonMobileTestInformation() {
    
	}
	
	/**
	 * @return the testId
	 */
	public long getTestId() {
		return testId;
	}

	/**
	 * @param testId the testId to set
	 */
	public void setTestId(long testId) {
		this.testId = testId;
	}

	public int getImagesPerPage() {
		return imagesPerPage;
	}

	public void setImagesPerPage(int imagesPerPage) {
		this.imagesPerPage = imagesPerPage;
	}

	public int getTimePerPage() {
		return timePerPage;
	}

	public void setTimePerPage(int timePerPage) {
		this.timePerPage = timePerPage;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public void setTime(long curentDateandTime) {
		this.time=curentDateandTime;
	}
	
	public long getTime(){
		return this.time;
	}
	
	public String getDate(){
		return DateFormat.getDateTimeInstance().format(new Date(this.time));
	}

	/**
	 * @return the sex
	 */
	public int getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(int sex) {
		this.sex = sex;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the educationLevel
	 */
	public int getEducationLevel() {
		return educationLevel;
	}

	/**
	 * @param educationLevel the educationLevel to set
	 */
	public void setEducationLevel(int educationLevel) {
		this.educationLevel = educationLevel;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the dpi
	 */
	public int getDpi() {
		return dpi;
	}

	/**
	 * @param dpi the dpi to set
	 */
	public void setDpi(int dpi) {
		this.dpi = dpi;
	}

	/**
	 * @return the resolution
	 */
	public String getResolution() {
		return resolution;
	}

	/**
	 * @param resolution the resolution to set
	 */
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	/**
	 * @return the orientation
	 */
	public int getOrientation() {
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	/**
	 * @return the cellSize
	 */
	public int getCellSize() {
		return cellSize;
	}

	/**
	 * @param cellSize the cellSize to set
	 */
	public void setCellSize(int cellSize) {
		this.cellSize = cellSize;
	}

	/**
	 * @return the user_id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the user_id to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the refImg1Id
	 */
	public int getRefImg1Id() {
		return refImg1Id;
	}

	/**
	 * @param refImg1Id the refImg1Id to set
	 */
	public void setRefImg1Id(int refImg1Id) {
		this.refImg1Id = refImg1Id;
	}

	/**
	 * @return the refImg2Id
	 */
	public int getRefImg2Id() {
		return refImg2Id;
	}

	/**
	 * @param refImg2Id the refImg2Id to set
	 */
	public void setRefImg2Id(int refImg2Id) {
		this.refImg2Id = refImg2Id;
	}

	/**
	 * @return the refImg3Id
	 */
	public int getRefImg3Id() {
		return refImg3Id;
	}

	/**
	 * @param refImg3Id the refImg3Id to set
	 */
	public void setRefImg3Id(int refImg3Id) {
		this.refImg3Id = refImg3Id;
	}
}
