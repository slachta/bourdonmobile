#!/bin/bash

#
# Converts an image from png to svg
#
# $1 width
# $2 input file path
# $3 export file path
#

convert_svg(){
	inkscape --export-png=$3 --export-width=$1 --export-height=$1 $2
}

#
# Rotates an image
#
# $1 path
# $2 iteration of rotation (90*f)
#

rotate_png(){
	convert $1 -rotate $((90 * $2)) $1
}

dimensions="144 192 240 288 336"
types="top corner"
subtypes="false normal selected true"

mkdir -p $(pwd)/output
mkdir -p $(pwd)/tmp
for dimension in $dimensions;
do
	echo Creating files for $dimension dimension.
	min=0
	max=3
	mkdir -p $(pwd)/output/$dimension/
	mkdir -p $(pwd)/tmp/$dimension/
	for t in $types;
	do
		for s in $subtypes;
		do
			convert_svg $dimension $(pwd)/img/"$t"_"$s".svg $(pwd)/tmp/"$dimension"/"$t"_"$s".png > /dev/null 2>&1

			for (( variant=$min; variant<=$max; variant++ ))
			do
				origin=$(pwd)/tmp/"$dimension"/"$t"_"$s".png
				target=$(pwd)/output/"$dimension"/im_"$variant"_"$s".png
				cp $origin $target
				rotate_png $target $variant
			done
		done
		min=$(( $max + 1))
		max=$(( $max + $max + 1))
	done
done

rm -rf $(pwd)/tmp/
