# README #

This is a source code repository for BourdonMobile application.

### How do I get set up? ###

* You can import the project into Android studio by clicking on "checkout from Version Control" button. The Vcs Repository URL should be "https://bitbucket.org/slachta/bourdonmobile.git"
* If you want to use Eclipse with ADT, the last commit for this IDE was 82b2c84cc55c8925a16c246f7410dbd0d9b0be62 or to download http://code.slachta.eu/bourdonmobile/downloads/slachta-bm-82b2c84cc55c.zip
